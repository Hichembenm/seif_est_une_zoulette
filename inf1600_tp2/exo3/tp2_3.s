.data
	
	i: .int 0
	

.global func_s

func_s:	
	
				incrementation_du_millenaire:
					mov i,%eax
					add $1,%eax
					mov $10,%ebx
					mov %eax,i
					cmp %eax,%ebx
					jnae quit
				
				mov d,%eax
				mov e,%ebx
				mov b,%ecx
				add %ebx,%eax
				sub %ecx,%eax
				mov %eax,a

				
				sub $1000,%ecx
				mov c,%esi
				add $500,%esi
				cmp %ecx,%esi
				jna else
				
				mov c,%eax
				sub $500,%eax
				mov %eax,c
				mov b,%ebx	
				cmp %eax,%ebx
				ja little_if
				jmp incrementation_du_millenaire
		
				little_if:
					mov b,%eax
					sub $500,%eax
					mov %eax,b	
					jmp incrementation_du_millenaire
			
				else:
					mov e,%eax
					mov b,%ebx	
					sub %eax,%ebx
					mov %ebx,b
					mov d,%eax		
					add $500,%eax		
					mov %eax,d
					
				jmp func_s		
				
				quit:
	
	ret




