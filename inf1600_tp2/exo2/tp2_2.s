
.global func_s

#Nous avons creer une variable poubelle (poub afin d'utiliser l'instruction fstps x sans modifier nos #variables (a,b,c,d,e,f,g)
func_s:
	/* Votre programme assembleur ici... */
	flds b
	flds d
	fmulp
	fstps poub
	flds c
	fsubp
	fstps poub
	flds f
	flds g
	fsubp
	fstps poub
	fdivrp
	fstps poub
	flds e
	faddp
	fstps poub
	flds g
	flds e
	fsubp
	fstps poub
	flds f
	fdivrp
	fstps poub
	fmulp
	fstps poub
	flds a
	fsubrp
	
ret
