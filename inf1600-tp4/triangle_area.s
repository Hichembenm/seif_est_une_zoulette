.data
	factor: .float 2.0 /* use this to multiply by two */


.globl _ZNK9CTriangle7AreaAsmEv

_ZNK9CTriangle7AreaAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        mov 8(%ebp),%ebx /* pointeur vers objet */
        mov (%ebx),%eax /*dans vtable*/
	sub $12,%esp /* allouer espace pour p, p-mSides[0],p-mSides[1] */
	push %ebx
  	call *8(%eax) /* Call PerimeterCpp */
	fld (factor)
	fdivrp		
	fstp -4(%ebp) /* variable p = area/2.0 */
	fld -4(%ebp) 
	fld 4(%ebx)
	fsubrp        /* p-mSides[0] */
	fstp -8(%ebp) 
	fld -4(%ebp)
	fld 8(%ebx)
	fsubrp /* p-mSides[1] */
	fstp -12(%ebp)
	fld -4(%ebp)
	fld 12(%ebx)
	fsubrp /* p - msides[2] */
	fld -12(%ebp)
	fmulp /* (p - msides[2]) * (p - msides[1]) */
	fld -8(%ebp)
	fmulp /* (p - msides[2]) * (p - msides[1]) * (p - msides[0]) */
	fld -4(%ebp)
	fmulp  
	fsqrt
	

        leave          
        ret            /* return to the caller */
		
