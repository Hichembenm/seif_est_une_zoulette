.globl _ZNK7CCircle7AreaAsmEv

_ZNK7CCircle7AreaAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
        movl 8(%ebp),%ebx
        fld 4(%ebx) /* mRadius */
        fld 4(%ebx) 
        fmulp /* mRadius * mRadius */
        fldpi /* mRadius * mRadius * PI */
        fmulp
        leave         
    
        ret            /* return to the caller */
