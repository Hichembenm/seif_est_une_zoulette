.globl _ZNK9CTriangle12PerimeterAsmEv

_ZNK9CTriangle12PerimeterAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */
	mov 8(%ebp),%ebx
	fld 4(%ebx)
	fld 8(%ebx)
	faddp /* mSides[0] + mSides[1] */
	fld 12(%ebx)
	faddp /* mSides[0] + mSides[1] + mSides[2] */
        
        leave        
        ret            /* return to the caller */
