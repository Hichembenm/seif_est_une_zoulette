.data
        factor: .float 2.0 /* use this to multiply by two */

.text
.globl	_ZNK9CTriangle9HeightAsmEv

_ZNK9CTriangle9HeightAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */
        
        /* Write your solution here */ 
       
        mov 8(%ebp),%ebx /* pointeur vers objet */
        mov (%ebx),%eax /*dans vtable*/
	sub $4,%esp /* espace pour A */
	push %ebx
        call *16(%eax)		/*call areacpp*/ 
        pop %ebx
	fstp -4(%ebp) /* variable A */
	fld -4(%ebp) 
        fld 12(%ebx)
        fdivrp /* A/mSides[2] */
        fld (factor)
        fmulp /* 2.0 * A/mSides[2] */
        leave          
       	
       
        ret            /* return to the caller */
