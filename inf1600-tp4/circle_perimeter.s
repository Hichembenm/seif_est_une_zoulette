.data
        factor: .float 2.0 /* use this to multiply by two */

.text
.globl _ZNK7CCircle12PerimeterAsmEv

_ZNK7CCircle12PerimeterAsmEv:
        push %ebp      /* save old base pointer */
        mov %esp, %ebp /* set ebp to current esp */

        /* Write your solution here */
        mov 8(%ebp),%ebx
        fld 4(%ebx) 
        fldpi
        fmulp /* mRadius * PI */
        fld (factor) 
        fmulp /* mRadius * PI * 2.0 */
	leave
        ret            /* return to the caller */
